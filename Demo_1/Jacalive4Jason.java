import jason.architecture.AgArch;

import java.io.IOException;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Hashtable;

import cartago.AgentId;
import cartago.ArtifactConfig;
import cartago.CartagoNode;
import cartago.CartagoWorkspace;
import cartago.WorkspaceKernel;
import JaCalIVEFrameWork.JaCalIVEFrameWork;
import JaCalIVEFrameWork.JaCalIVEWorkSpace;

public class Jacalive4Jason extends JaCalIVEFrameWork {
	
	static JaCalIVEWorkSpace jacalIVE_WorkSpace = new JaCalIVEWorkSpace();
	
	CartagoWorkspace cartWork;
	ArtifactConfig ConfigArti;
	CartagoNode Node;
	WorkspaceKernel WorkSpaKernel;
	java.util.Set<String> list = null;
	static String[] args1 = new String[17];
	
	static JaCalIVEFrameWork jacalIVE;
	
	float[] PosXYZ = {0f, 0f, 0f};
	float[] Angle_WXYZ = {0f, 0f, 0f, 0f};
	
	int iPosiX = 0;
	int iPosiY = 0;
	int iPosiZ = 0;
	String sPosiX = "";
	
	String sPosXYZ = " ";
	
	
	public Jacalive4Jason() {
		new Hashtable<String, AgentId>();
	}

	public void initJacaLive(String sName, int numWsP, int TamaLinkArtifact, int TamaUnLinkArtifact, int TamaBodyArti, double Gx, double Gy,
			double Gz, int Wmap, int Hmap, int Lmap, boolean bGravity, float fGroundFriction, String Floor, int HostPort, String HostName, String RendeOnOff) throws Exception {

			/** initialize de Nodos, Workspace etc **/
		Node = CartagoNode.getInstance();
		/** Config Artifact **/
		ConfigArti = new ArtifactConfig();
		/** Create the workspace **/
		cartWork = Node.createWorkspace(sName);
		
		WorkSpaKernel = cartWork.getKernel();
		AgArch userAgArch = new AgArch();
		int iId = 0;
		System.out.println("-----------------------------------------------");
		ClassLoader cl = ClassLoader.getSystemClassLoader();
		URL[] urls = ((URLClassLoader) cl).getURLs();
		for (URL url : urls) {
			System.out.println(url.getFile());
		}
		System.out.println("-----------------------------------------------");
		
		args1[0] = sName;
		args1[1] = Integer.toString(numWsP);
		args1[2] = Integer.toString(TamaLinkArtifact);
		args1[3] = Integer.toString(TamaUnLinkArtifact);
		args1[4] = Integer.toString(TamaBodyArti);
		args1[5] = Double.toString(Gx);
		args1[6] = Double.toString(Gy);
		args1[7] = Double.toString(Gz);
		args1[8] = Integer.toString(Wmap);
		args1[9] = Integer.toString(Hmap);
		args1[10] = Integer.toString(Lmap);
		args1[11] = Boolean.toString(bGravity);
		args1[12] = Double.toString(fGroundFriction);
		args1[13] = Floor;
		args1[14] = Integer.toString(HostPort);
		args1[15] = HostName;
		args1[16] = RendeOnOff;
				
		main(args1);
		// Creation and initialization of Workspace
		jacalIVE_WorkSpace.iniWsp(Node, ConfigArti, cartWork, WorkSpaKernel, userAgArch);
	}
	
	/*public void activateRender(boolean FlagRender)throws Exception{
		activateRenderToJaCalIVE();
		if(FlagRender==true){			
			viewRender(FlagRender);
		}
	}*/	

	public String getAngleBodyArtifact(String name){
		Angle_WXYZ = getAngleBodyArtifacts(name);
		String sAngle_WXYZ = Float.toString(Angle_WXYZ[0]) + ", " + Float.toString(Angle_WXYZ[1]) + ", " + Float.toString(Angle_WXYZ[2]) + ", " + Float.toString(Angle_WXYZ[3]);
		return sAngle_WXYZ;		
	}
	
	
	public String getPosOfBodyArtifact(String name){
		PosXYZ = getPosOfBodyArtifacts(name);			
		sPosXYZ = Float.toString(PosXYZ[0]) + ", " + Float.toString(PosXYZ[1]) + ", " + Float.toString(PosXYZ[2]);		
		return sPosXYZ;
	}
	
	public void getColicion_Flag(String nameAg){
		System.out.println(getColicionFlags(nameAg));
	}
	
	public void setNameBodys(String name, int index){
		System.out.println("name: " + name + " ; index: "+ index);
		setNameBodyArtifact(name, index);
	}
				
	public void createBodys(){
		createBody();
	}	
	
	public void timeDelays()  throws InterruptedException{
		timeDelay();
	}
	
	public void getSizeOfArtifacts(){
		System.out.println(getNumOfArtifacts());
	}
	
	public Boolean sendRender(String data) throws IOException {
		return sendRenders(data);
	}
	
	public void inObsProperty(String Type, String Name, double L, double W, double H, 
		double Mass, double AccelerationX, double AccelerationY,
		double AccelerationZ, String Sound, String Shape, double Angle,
		double Distance, int JointX, int JointY, int JointZ,
		double OrientationX, double OrientationY, double OrientationZ,
		double VelocityX, double VelocityY, double VelocityZ, double Px,
		double Py, double Pz) {
				
		Object[] param = {Type, Name, L, W, H, Mass, AccelerationX, AccelerationY,
			 AccelerationZ,  Sound,  Shape,  Angle,
			 Distance,  JointX,  JointY,  JointZ,
			 OrientationX,  OrientationY,  OrientationZ,
			 VelocityX,  VelocityY,  VelocityZ,  Px,
			 Py,  Pz };
		inputObsProperty(param);
	}
	
	public String splitInformation(String Data, String Separator, int I){
		String[] parts = Data.split(Separator);
		
		return parts[I];
	}
	
	
	// SDA	
	public void multiClientServerSDA() throws Exception{
		initServerSDA();		
	}
	
	public String getSDAAddres(String nameSDA){
		return get_SDA_Addres();
	}
	
	public void sendDataSDA(String Data){
		sendData_SDA(Data);
	}
	
	// Distributed
	public void initDistributed(){
		initDistributedServer();
	}
	
	//******************************
	public void readTxtFiles(String fileName) throws IOException{
		readFileTXT(fileName);		
	}
	
	public void readPythonScript(String Args, String Path) throws IOException{
		String Data = runPythonScript(Args, Path);		
		
		System.out.println("Python Data: " + Data);
	}
}

