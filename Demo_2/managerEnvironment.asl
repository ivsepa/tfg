
/* Initial goals */
!start.

+!start : true <-	
	.print("Hi I'm the Manager Environment'.");
	//ivanTest.IvanAction("Hola", "Que tal", "3", "Pues aqui, testeando");
	//websocket.IniciarServer(WSserver);
	//websocket.Handshake(WSserver);
	
	// Inicializacion de las Variables del Mundo
	Gx = 0.0;// Gravedad en X 
    Gy = -9.8;// Gravedad en Y 
    Gz = 0.0;// Gravedad en Z 
	Bgravity = true;
	U = 0.5;
	
	Width = 1;
	Height = 100;
	Length = 100;

	/* This is a possition of all body artifact*/
	Body_X = [15.0];
	Body_Y = [15.0];
	Body_Z = [15.0];

	/* Environment elementes */
	
	
    createWorkspace("emotion_workspace"); 
    joinWorkspace("emotion_workspace",WspID0); 
    cartago.set_current_wsp(WspID0); 
	
	
	// Creatin artifact
	//!setupArtifact2(AId2, 1.0,1.0,1.0,20.0,1.0,0.0,0.0,"none","cube",0.0,5.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.1,-300.0,25.0,-250.0);
	//.print(AId1);
	!setupArtifact1(AId1, 1.0,1.0,1.0,20.0,1.0,0.0,0.0,"none","cube",0.0,5.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.1,0.0,5.0,-125.0);
	!setupArtifact2(AId2, 1.0,1.0,1.0,20.0,1.0,0.0,0.0,"none","sphere",0.0,5.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.1,20.0,5.0,-75.0);
	!setupArtifact3(AId3, 1.0,1.0,1.0,20.0,1.0,0.0,0.0,"none","capsule",0.0,5.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.1,-20.0,5.0,-100.0);
	//!setupArtifact1(AId1, 1.0,1.0,1.0,20.0,1.0,0.0,0.0,"none","sphere",0.0,5.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.1,0.0,25.0,250.0);
	//!setupArtifact3(AId3, 1.0,1.0,1.0,20.0,1.0,0.0,0.0,"none","capsule",0.0,5.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,1.0,1.1,300.0,50.0,0.0);
	
	/* Concatenamos los nombres de de todos los Work Space */ 
	NamesWorkSpaces ="emotion_workspace";
	
    TamaBodyArti = 3;
    TamaLinkArtifact = 0;
	TamaUnLinkArtifact = 0;
	
	 cartago.new_obj("Jacalive4Jason",[],InitJacalIve);
	 cartago.new_obj("websocket.WebsocketServer",[], ServidorWS);	//WEBSOCKET
	 //makeArtifact("ServidorWebsocket", "WebsocketServer",[],ServidorWS); //cartago.UnknownArtifactTemplateException
	 +server(ServidorWS);											//WEBSOCKET
	 
	 /*cartago.invoke_obj(Servidor, getTesteo, CadenaTest);
	 .print("TEST: ", CadenaTest);
	 cartago.invoke_obj(Servidor, setTesteo("Probando"));
	 cartago.invoke_obj(Servidor, getTesteo, CadenaTest2);
	 .print("TEST: ", CadenaTest2);*/
	 /*.concat("127.0.0.1", IP);
	 cartago.invoke_obj(ServidorWS, handshake(IP));*/
	 cartago.invoke_obj(ServidorWS, inicializacion);				//WEBSOCKET
	//inicializacion; //cartago.UnknownArtifactTemplateException
	 
	 cartago.invoke_obj(InitJacalIve, initJacaLive(emotion_workspace, 1, TamaLinkArtifact, TamaUnLinkArtifact, TamaBodyArti, Gx, Gy, Gz, Width, Height, Length, Bgravity, U, "Floor", 9050, "127.0.0.1", "true"));
	 
	 //cartago.invoke_obj(InitJacalIve, initJacaLive(emotion_workspace, 100, TamaLinkArtifact, TamaUnLinkArtifact, TamaBodyArti, Gx, Gy, Gz, Width, Height, Length, Bgravity, U, "Floor", 9050, "192.168.1.14", "true"));
	 //cartago.invoke_obj(InitJacalIve, initJacaLive(emotion_workspace, 100, TamaLinkArtifact, TamaUnLinkArtifact, TamaBodyArti, Gx, Gy, Gz, Width, Height, Length, Bgravity, U, "Floor", 4446, "127.0.0.1", "true"));
	
	 //cartago.invoke_obj(InitJacalIve, readTxtFiles("map_1.txt"));
	 
	 
	.all_names(Name); 	
	.difference(Name,[managerEnvironment], T);
	.print("Names of Agents: ", T);
	.length(T, Tama);
	
 	for(.range(I,0,Tama-1)){ 
		.nth(I,T,X); 
		.send(X, achieve, configBody("1"), 100); 
		.wait(200);
	} 		
	cartago.invoke_obj(InitJacalIve,createBodys);
	
	.print ("End...");
	.wait(3000);
	
	// Start Multi Client SDA
	cartago.invoke_obj(InitJacalIve,multiClientServerSDA);
	
	// start distributed server
	cartago.invoke_obj(InitJacalIve, initDistributed);
	
	cartago.invoke_obj(InitJacalIve, readPythonScript("0", "../PythonScript/Script.py"));
	
	
	!getDataAgents.
	
// Creation of artifact
+!setupArtifact2(AId2, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz ): true
<-	cartago.new_obj("Jacalive4Jason",[],Id2);
	cartago.invoke_obj(Id2, inObsProperty("IVE_Artifact", Shape, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz));
	makeArtifact("IVE_Artifact","IVE_Artifact_class",[L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz ], AId2);
	focus(AId2).

+!setupArtifact1(AId1, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz ): true
<-	cartago.new_obj("Jacalive4Jason",[],Id1);
	cartago.invoke_obj(Id1, inObsProperty("IVE_Artifact", Shape, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz));
	makeArtifact("IVE_Artifact1","IVE_Artifact1_class",[L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz ], AId1);
	focus(AId1).
	
+!setupArtifact3(AId3, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz ): true
<-	cartago.new_obj("Jacalive4Jason",[],Id3);
	cartago.invoke_obj(Id3, inObsProperty("IVE_Artifact", Shape, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz));
	makeArtifact("IVE_Artifact3","IVE_Artifact3_class",[L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz ], AId3);
	focus(AId3).


	
+!jacalIVE_Loop(Data)[source(Ag)]: true
<-	cartago.new_obj("Jacalive4Jason",[],IVE_Loop);
	?server(ServidorWS);    //WEBSOCKET

	//cartago.invoke_obj(IVE_Loop, getPosOfBodyArtifact("agent_1"), PosXYZ);
	cartago.invoke_obj(IVE_Loop, getPosOfBodyArtifact(Ag), PosXYZ);
	//cartago.invoke_obj(IVE_Loop, getPosOfBodyArtifact("agent_2"), PosXYZ2);
	//cartago.invoke_obj(IVE_Loop, getPosOfBodyArtifact("agent_3"), PosXYZ3);
	
	//cartago.invoke_obj(IVE_Loop, getAngleBodyArtifact("agent_1"), Angle_WXYZ);
	cartago.invoke_obj(IVE_Loop, getAngleBodyArtifact(Ag), Angle_WXYZ);
	//cartago.invoke_obj(IVE_Loop, getAngleBodyArtifact("agent_2"), Angle_WXYZ2);
	//cartago.invoke_obj(IVE_Loop, getAngleBodyArtifact("agent_3"), Angle_WXYZ3);
    cartago.invoke_obj(IVE_Loop, timeDelays);

	cartago.invoke_obj(IVE_Loop, splitInformation(Data, ",", 7), SDA_Name);
	cartago.invoke_obj(IVE_Loop, splitInformation(Data, ",", 8), SDA_Data);
	
	//.print(SDA_Name);
	//.print(SDA_Data);
	.concat("", Ag," pos = ",PosXYZ, Posicion);
	//.print(PosXYZ);
	.print(Posicion);
	//cartago.invoke_obj(IVE_Loop, getColicion_Flag("agent_1"));
	
	//.concat(BodyAg, " ", SPosRobot ," ", "Link_Obj_Artifact", " ", SLinkArti , " ", "Unlink_Wall_Artifact", " ", SUnLinkArti, Pack_7);
	
	//cartago.invoke_obj(IVE_Loop, readPythonScript("0", "../PythonScript/Script.py"));
	
	//Inhabitant,cube,agent_1,0.5,28,33,0
	
	//.concat("Inhabitant,cube,agent_1,",PosXYZ, ",", Angle_WXYZ, Pack_8);
	.concat("", Ag,",", PosXYZ, ",", Angle_WXYZ, Pack_8);
	//.concat("Inhabitant,sphere,agent_2,",PosXYZ2, ",", Angle_WXYZ2, Pack_82);
	//.concat("HumanInmersedAgent,capsule,agent_3,",PosXYZ3, ",", Angle_WXYZ3, Pack_83);
	//.print(Pack_8);
	
	
	//cartago.invoke_obj(IVE_Loop, getSDAAddres("SDA_0X02"), SDA_Addres);
	//.print(SDA_Addres);
	
	//.concat(SDA_Name," ", SDA_Data ,SDA_Data_Send);
	//cartago.invoke_obj(IVE_Loop, sendDataSDA(SDA_Data_Send));
	//.wait(500);
	
	//cartago.invoke_obj(IVE_Loop, sendDataSDA("QUIT"));
	//.wait(500);
	cartago.invoke_obj(IVE_Loop,sendRender(Pack_8), Pack_7); 
	cartago.invoke_obj(ServidorWS, sendWebsocketMessage(Pack_8));	//WEBSOCKET
	//sendWebsocketMessage(Pack_8); //cartago.UnknownArtifactTemplateException
	
	//cartago.invoke_obj(IVE_Loop,sendRender(Pack_82), Pack_72);
	//cartago.invoke_obj(IVE_Loop,sendRender(Pack_83), Pack_73);
	
	
	/*.concat("Inhabitant,cube,agent_1,",PosXYZ, Pack_mov1);
	.concat("Inhabitant,sphere,agent_2,",PosXYZ2, Pack_mov2);
	.concat("HumanInmersedAgent,capsule,agent_3,",PosXYZ3, Pack_mov3);
	
	cartago.invoke_obj(IVE_Loop,sendRender(Pack_mov1), Pack_Movb1);
	cartago.invoke_obj(IVE_Loop,sendRender(Pack_mov2), Pack_Movb2);
	cartago.invoke_obj(IVE_Loop,sendRender(Pack_mov3), Pack_Movb3);*/

	!getDataAgents.

	
+!getDataAgents: true
<-	.all_names(Name); 	
	.difference(Name,[managerEnvironment], T);
	.length(T, Tama); 
	
 	for(.range(I,0,Tama-1)){ 
		.nth(I,T,X); 
		.send(X, achieve, getInfo("1")); 
	} .
		
	
+!configureBody(Data, Index)[source(Ag)] : true
<-	cartago.new_obj("Jacalive4Jason",[],NameBodys);
    cartago.invoke_obj(NameBodys,setNameBodys(Data, Index)).

	
	
