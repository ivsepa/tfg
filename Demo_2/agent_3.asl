  	  
+!configBody(X)[source(Ag)]: true
<-	if(X=="1"){
		!getBody;
	}else{
		!configArtifac;
	}.

+!getInfo(Data)[source(Ag)]: true
<-
	//if(Data=="1"){
		Type = "HumanInmersedAgent";
		SName = "agent_3";
		Sangle = "0.5";
		X1 = "25";
		Y1 = "23";
		Z1 = "0";
		Shape = "capsule";
		SDA = "SDA_0X04";
		SDA_Action = "128 200 50";
		.concat(Type, ",", Shape, ",", SName, ",", Sangle, ",", X1, ",", Y1, ",", Z1, "," , SDA, ",", SDA_Action, Ssend);
		.send(managerEnvironment, achieve, jacalIVE_Loop(Ssend));
	//}
	.
	
	
/*---------------------------------------------------------------------------*/	
/*Accedo a los Atefactos y los envio al entorno virtual						 */
/*---------------------------------------------------------------------------*/
+!getBody
<-	
/*---------------------------------------------------------------------------*/    
/* comunicacion entre el agente robot y el manager, para que el manager le   */
/* adjudique un cuerpo														 */
/*---------------------------------------------------------------------------*/
	Type = "Inhabitan";
	//Type = "HumanInmersedAgent";
	.my_name(NameOfMyBody);
	.term2string(NameOfMyBody, Mn);
	XA = 20+5;
	YA = 20+3;
	ZA = 0; 
	Mass = 1;
	SizeX = 1;
	SizeY = 1;
	SizeZ = 1;
	Body = "capsule";
	
	Index = 2;
	.concat(Type, ",", Mn, ",", XA, ",", YA, ",", ZA, ",", Mass, ",", SizeX, ",", SizeY, ",", SizeZ, ",", Body, AgentInfo);
	
	.send(managerEnvironment, achieve, configureBody(AgentInfo, Index));
	.wait(1000).
	
