﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;

public class EchoTest : MonoBehaviour {

    public GameObject mapa1;

    public GameObject agente1;
    public GameObject agente2;
    public GameObject agente3;

    //IsolatedStorageException: Could not find a part of the path "/Users/ivsep/workspaceBarrachina/renderWebGL/log.txt"
    //    private string nombreArchivo = "/Users/ivsep/workspaceBarrachina/renderWebGL/log.txt";

    private string nombreArchivo = "C:\\Users\\ivsep\\workspaceBarrachina\\renderWebGL\\log.txt";
    private string logString = "";

    // Use this for initialization
    IEnumerator Start () {
       
        //WebSocket w = new WebSocket(new Uri("ws://echo.websocket.org"));
        WebSocket w = new WebSocket(new Uri("ws://localhost:7777"));
        yield return StartCoroutine(w.Connect());
		w.SendString("Mensaje desde el cliente web");
        Debug.Log("saludo enviado");

        float tiempo = 0.0f;

       // TextWriter archivo = new StreamWriter(nombreArchivo, true);
       //var archivo = File.CreateText(nombreArchivo);
        tiempo += Time.deltaTime;
        /* Debug.Log("Escribiendo en " + nombreArchivo);
         File.AppendAllText(nombreArchivo, "##############################################" + Environment.NewLine);
         File.AppendAllText(nombreArchivo, "##############################################" + Environment.NewLine);
         File.AppendAllText(nombreArchivo, "##############################################" + Environment.NewLine);*/
        //        File.AppendAllText(nombreArchivo, tiempo + Environment.NewLine);
        logString = logString + tiempo + Environment.NewLine;
       // archivo.WriteLine("Empezamos con el tiempo en " + tiempo + "\n");

        // archivo.Close();


        while (true)
		{
            //Debug.Log("esperando respuesta");
			string reply = w.RecvString();
			if (reply != null)
			{
                tiempo += Time.deltaTime;
                //archivo = File.CreateText(nombreArchivo);
                //  archivo = new StreamWriter(nombreArchivo, true);
                //                File.AppendAllText(nombreArchivo, tiempo + Environment.NewLine);
                logString = logString + tiempo + Environment.NewLine;
                //archivo.WriteLine("mensaje recibido en " + tiempo + "\n");
                //  archivo.Close();
                //OK,1,Floor,1,100,100,3,

                //que es cada cosa está en managerEnvironment.asl, linea 84 +-
                //IVE_Artifact,  AId2, L, W, H, Mass, AccelerationX, AccelerationY, AccelerationZ, Sound,  Shape, Angle, Distance, JointX, JointY, JointZ, OrientationX, OrientationY, OrientationZ, VelocityX, VelocityY, VelocityZ, Px, Py, Pz
                //IVE_Artifact,cube,1.0,1.0,1.0,20.0,1.0,0.0,0.0,none,cube,0.0,5.0,1,0,0,1.0,0.0,0.0,1.0,1.0,1.1,-300.0,25.0,-250.0,
                //IVE_Artifact,sphere,1.0,1.0,1.0,20.0,1.0,0.0,0.0,none,sphere,0.0,5.0,1,0,0,1.0,0.0,0.0,1.0,1.0,1.1,0.0,25.0,250.0,
                //IVE_Artifact,capsule,1.0,1.0,1.0,20.0,1.0,0.0,0.0,none,capsule,0.0,5.0,1,0,0,1.0,0.0,0.0,1.0,1.0,1.1,300.0,50.0,0.0
                string[] tokens = reply.Split(',');
                if (tokens[0].Equals("OK"))
                {
                    string nombreMapa = tokens[2];
                    float altoMapa = float.Parse(tokens[3]);
                    float anchoMapa = float.Parse(tokens[4]);
                    float largoMapa = float.Parse(tokens[5]);

                    mapa1 = Instantiate(mapa1, new Vector3(0, -1, 0), Quaternion.identity);
                    mapa1.transform.localScale = new Vector3(anchoMapa, altoMapa, largoMapa);


                    string[] stringSeparators = new string[] { "IVE_Artifact" };
                    string[] agentes = reply.Split(stringSeparators, StringSplitOptions.None);
                    for(int i = 1; i < agentes.Length; i++)
                    {
                        
                        string[] agente = agentes[i].Split(',');
                        //tipo de agente
                        string idAgente = agente[1];
                        //escala
                        float scaleX = float.Parse(agente[2]);
                        float scaleZ = float.Parse(agente[3]);
                        float scaleY = float.Parse(agente[4]);
                        //masa
                        float masa = float.Parse(agente[5]);
                        //aceleracion
                        float aceX = float.Parse(agente[6]);
                        float aceY = float.Parse(agente[7]);
                        float aceZ = float.Parse(agente[8]);
                        //sonido y forma
                        string sonido = agente[9];
                        string forma = agente[10];
                        //angulo y distancia
                        float angulo = float.Parse(agente[11]);
                        float distancia = float.Parse(agente[12]);
                        //joint
                        float jointX = float.Parse(agente[13]);
                        float jointY = float.Parse(agente[14]);
                        float jointZ = float.Parse(agente[15]);
                        //orientacion
                        float oriX = float.Parse(agente[16]);
                        float oriY = float.Parse(agente[17]);
                        float oriZ = float.Parse(agente[18]);
                        //velocidad
                        float velX = float.Parse(agente[19]);
                        float velY = float.Parse(agente[20]);
                        float velZ = float.Parse(agente[21]);
                        //posicion
                        float posX = float.Parse(agente[22]);
                        float posY = float.Parse(agente[23]);
                        float posZ = float.Parse(agente[24]);
                        switch (forma)
                        {
                            case "cube":
                                agente1 = Instantiate(agente1, new Vector3(posX, posY, posZ), Quaternion.Euler(new Vector3(oriX, oriY, oriZ)));
                                break;
                            case "sphere":
                                agente2 = Instantiate(agente2, new Vector3(posX, posY, posZ), Quaternion.Euler(new Vector3(oriX, oriY, oriZ)));
                                break;
                            case "capsule":
                                agente3 = Instantiate(agente3, new Vector3(posX, posY, posZ), Quaternion.Euler(new Vector3(oriX, oriY, oriZ)));
                                break;
                            default:
                                
                                break;
                        }
                    }
                }
                else if(tokens[0].StartsWith("agent_"))
                {
                    //Received: agent_3: pos = 21.461454, 21.25119, 0.0982475, angle WXYZ = 0.46175548, 0.8223119, 0.2909488, 0.16104011
                    //argumento 0: nombre del agente
                    //argumentos 1, 2, 3: posicion X, Y, Z del agente
                    //argumentos 4, 5, 6, 7: angulo W, X, Y, Z del agente
                    //agent_1,28.0, 32.714165, 0.0,1.0, 0.0, 0.0, 0.0
                    string nombreAgente = tokens[0];
                    float posX = float.Parse(tokens[1]);
                    float posY = float.Parse(tokens[2]);
                    float posZ = float.Parse(tokens[3]);
                    float angW = float.Parse(tokens[4]);
                    float angX = float.Parse(tokens[5]);
                    float angY = float.Parse(tokens[6]);
                    float angZ = float.Parse(tokens[7]);
                    switch (nombreAgente)
                    {
                        case "agent_1":
                            //agente1.transform.position = new Vector3(posX, posY, posZ);
                            agente1.transform.SetPositionAndRotation(new Vector3(posX, posZ, posY), Quaternion.Euler(new Vector3(angX, angY, angZ)));
                            break;
                        case "agent_2":
                            //agente2.transform.position = new Vector3(posX, posY, posZ);
                            agente2.transform.SetPositionAndRotation(new Vector3(posX, posZ, posY), Quaternion.Euler(new Vector3(angX, angY, angZ)));
                            break;
                        case "agent_3":
                            //agente3.transform.position = new Vector3(posX, posY, posZ);
                            agente3.transform.SetPositionAndRotation(new Vector3(posX, posZ, posY), Quaternion.Euler(new Vector3(angX, angY, angZ)));
                            break;
                    }

                }
                Debug.Log ("Received: "+reply);
                /*GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.AddComponent<Rigidbody>();
                cube.transform.position = new Vector3(0, 5, 0);*/

                //w.SendString("Hola "+i++);
			}
			if (w.error != null)
			{
                
				Debug.LogError ("Error: "+w.error);
				break;
			}
			yield return 0;
		}
		w.Close();
       
    }

    private void OnGUI()
    {
        GUI.TextArea(new Rect(10, 10, 150, 400), logString);
        /*if(GUI.Button(new Rect(10, 430, 50, 30), "Copiar"))
        {
            //GUIUtility.systemCopyBuffer = logString;
           

        }*/
    }
}
