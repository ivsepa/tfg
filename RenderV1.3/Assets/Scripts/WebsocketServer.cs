﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class WebsocketServer : MonoBehaviour {

    string guiServer = "";
    string guiWebSockets = "";
    string guiWellcome = "";
    string guiMensaje = "";

	// Use this for initialization
	void Start () {
        // NetworkServer.useWebSockets = true;
        /*NetworkManager nm = NetworkManager.singleton;
        nm.StartServer();*/
        NetworkManager.singleton.StartServer();
        guiServer = "" + NetworkServer.active;
        Debug.Log(guiServer);

        guiWebSockets = "" + NetworkServer.useWebSockets;
        Debug.Log(guiWebSockets);

        NetworkServer.RegisterHandler(MsgType.Connect, OnConnected);
        NetworkServer.RegisterHandler(25971, OnMessage);
        //NetworkServer.Listen(7777);
        NetworkManager.singleton.StartServer();

    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 150, 50), "NetworkServer.active: ");
        GUI.TextField(new Rect(170, 10, 100, 50), guiServer);

        GUI.Label(new Rect(10, 70, 150, 50), "NetworkServer.useWebSockets: ");
        GUI.TextField(new Rect(170, 70, 100, 50), guiServer);

        GUI.Label(new Rect(10, 130, 150, 50), "onConnect: ");
        GUI.TextField(new Rect(170, 130, 100, 50), guiWellcome);

        GUI.Label(new Rect(10, 190, 150, 50), "Mensaje: ");
        GUI.TextField(new Rect(170, 190, 100, 50), guiMensaje);
    }

    void OnConnected(NetworkMessage netMsg)
    {
        guiWellcome = "Client connected";
        Debug.Log(guiWellcome);
    }

    void OnMessage(NetworkMessage netMsg)
    {
        Debug.Log("Mensaje recibido");
        var mensaje = netMsg.ReadMessage<StringMessage>();
        guiMensaje = "mensaje = " + mensaje.value;
        Debug.Log(guiMensaje);
    }

}
