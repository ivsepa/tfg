package websocket;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

/**
 * Esta clase hereda de Thread y su funcion es conectarse al cliente web mediante websocket, 
 * y ponerse a la espera en un bucle while true, en el que va comprobando si tiene mensajes en 
 * la cola, que es un ArrayList, y los va enviando
 * @author SherN
 *
 */
public class WebsocketServer extends Thread{
	//public static WebsocketServer miServer;
	//public String testeo = "vacio";
	private ArrayList<String> colaMensajes;
	
	
	/*public void setTesteo(String cadena) {
		testeo = cadena;
	}
	public String getTesteo() {
		return testeo;
	}*/
	
	/*public void handshake(String ip) {
		System.out.println("IP: " + ip);
	}*/
	
	/**
	 * inicializa la cola de mensajes y arranca el hilo
	 */
	public void inicializacion() {
		colaMensajes = new ArrayList<String>();
		this.start();
	}
	
	/**
	 * Este metodo agrega el mensaje a la cola
	 */
	public void sendWebsocketMessage(String mensaje) {
		colaMensajes.add(mensaje);
	}
	
	/**
	 * Formatea los bytes que corresponden a la longitud del mensaje.
	 * 
	 * @param longitud
	 * @return Devuelve el resultado en un String que representa los bytes en
	 *         binario.
	 */
	public static String encodePayloadLength(int longitud) {
		if (longitud <= 125) {
			String res = Integer.toBinaryString(longitud);
			while (res.length() < 8) {
				res = "0" + res;
			}
			return res;

		} else if (longitud <= 65535) {
			// 16 bits
			String aux = Integer.toBinaryString(126);
			while (aux.length() < 8) {
				aux = "0" + aux;
			}
			String res = Integer.toBinaryString(longitud);
			while (res.length() < 16) {
				res = "0" + res;
			}
			return aux + res;

		} else {
			// 64 bits
			String aux = Integer.toBinaryString(127);
			while (aux.length() < 8) {
				aux = "0" + aux;
			}
			String res = Integer.toBinaryString(longitud);
			while (res.length() < 64) {
				res = "0" + res;
			}
			return aux + res;
		}
	}
	
	/**
	 * Traduce a array de bytes un String
	 * 
	 * @param origen
	 * @return
	 */
	public static byte[] stringToByteArray(String origen) {
		int numBytes = origen.length() / 8;
		byte[] res = new byte[numBytes];

		for (int i = 0; i < res.length; i++) {
			int val = Integer.parseInt(origen.substring(i * 8, i * 8 + 8), 2);
			res[i] = (byte) val;
		}

		return res;
	}
	
	public void run() {
		ServerSocket server;
		try {
			server = new ServerSocket(7777);
			System.out.println("InetAddress: " + server.getInetAddress());
			System.out.println("LocalSocketAddress: " + server.getLocalSocketAddress());
			System.out.println("Esperando conexi�n...");

			Socket client = server.accept();
			System.out.println("Cliente conectado.");

			InputStream in = client.getInputStream();
			OutputStream out = client.getOutputStream();
			String data = new Scanner(in, "UTF-8").useDelimiter("\\r\\n\\r\\n").next();
			Matcher get = Pattern.compile("^GET").matcher(data);

			if (get.find()) {
				System.out.println("data = " + data);
				Matcher match = Pattern.compile("Sec-WebSocket-Key: (.*)").matcher(data);
				match.find();
				System.out.println("match.group(1): " + match.group(1));
				System.out.println("generando response");
				byte[] response = ("HTTP/1.1 101 Switching Protocols\r\n" + "Connection: Upgrade\r\n"
						+ "Upgrade: websocket\r\n" + "Sec-WebSocket-Accept: "
						+ DatatypeConverter.printBase64Binary(MessageDigest.getInstance("SHA-1")
								.digest((match.group(1) + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes("UTF-8")))
						+ "\r\n\r\n").getBytes("UTF-8");
				System.out.println("enviando response");
				out.write(response, 0, response.length);
			} else {

			}
			// FIN DEL HANDSHAKE
			/*System.out.println("---------------------------------");
			System.out.println("ESPERANDO MENSAJES");
			System.out.println("---------------------------------");*/
			
			//recepcion UDP del mensaje inicial
			try {
				//mensajeInicial();
				int port = 9050;
				InetSocketAddress direccion = new InetSocketAddress("127.0.0.1", port);
				// Create a socket to listen on the port.
				DatagramSocket dsocket = new DatagramSocket();
				dsocket.setReuseAddress(true);
				dsocket.connect(direccion);

				// Create a buffer to read datagrams into. If a packet is larger than this buffer, the
				// excess will simply be discarded!
				byte[] buffer = "Render Jacalive Init".getBytes();

				// Create a packet to receive data into the buffer
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				dsocket.send(packet);
				System.out.println("ENVIADO Render Jacalive init");
				
				buffer = new byte[500];
				packet = new DatagramPacket(buffer, buffer.length);
				// Now loop forever, waiting to receive packets and printing them.
				String msg = "";
				String anterior = "";
				//flagUDP indica si he de seguir recibiendo mensajes UDP. Cuando reciba el mensaje inicial, paro.
				boolean flagUDP = true;
				
				while (flagUDP) {
					// Wait to receive a datagram
					dsocket.receive(packet);

					// Convert the contents to a string, and display them
					msg = new String(buffer, 0, packet.getLength());
					if (msg.equals(anterior)) {
						//si el mensaje que recibo es igual al anterior, no me interesa, no hago nada
					}else {
						//System.out.println(packet.getAddress().getHostName() + ": " + msg);
		
						// Reset the length of the packet before reusing it.
						if(msg.startsWith("OK")) {
							sendWebsocketMessage(msg);
							flagUDP = false;
						}
						anterior = msg;
						packet.setLength(buffer.length);
						dsocket.send(packet);
					}
					/*packet.setLength(buffer.length);
					dsocket.send(packet);*/
				}
			} catch (Exception e) {
				System.err.println(e);
			}
			
			int i = 0;
			while (true) {
				//si tengo algun mensaje que enviar, lo envio, si no, pues nada
				System.out.print(""); // si no pongo un print, el programa deja de enviar mensajes, ni idea de por que
				if (!colaMensajes.isEmpty()) {

					String mensaje = colaMensajes.remove(0);
					if(mensaje == null) {
						System.out.println("----------------------------------------------------------------------");
						System.out.println("IBA A CARGAR UN MENSAJE NULO");
						System.out.println("----------------------------------------------------------------------");
					}else {
						String paquete = "10000001" + encodePayloadLength(mensaje.length());
	
						byte[] parte1 = stringToByteArray(paquete);
						byte[] parte2 = mensaje.getBytes("UTF-8");
	
						byte[] response = new byte[parte1.length + parte2.length];
						System.arraycopy(parte1, 0, response, 0, parte1.length);
						System.arraycopy(parte2, 0, response, parte1.length, parte2.length);
	
						out.write(response, 0, response.length);
						/*escribe(i + " mensajes enviados");
						i++;*/
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public String toString() {
		return "HOLA SOY EL SERVER";
	}
}
